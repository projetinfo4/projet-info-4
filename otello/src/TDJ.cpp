#include "TDJ.h"

TDJ::TDJ()
{
    //ctor
}
TDJ::TDJ(std::vector<std::vector <int> >_plateau,int _tourJoueur):
    plateau(_plateau),tourJoueur(_tourJoueur)
{

}

TDJ::~TDJ()
{
    //dtor
}

int TDJ::getTourJoueur()
{
    return tourJoueur;
}

void TDJ::setJoueur(int joueur)
{
    tourJoueur=joueur;
}

void TDJ::swapTourJoueur()
{
    tourJoueur=(tourJoueur==1)?2:1;
}

void TDJ::setTableau(std::vector<std::vector <int> > tableau)
{
    plateau=tableau;
}

std::vector<std::vector <int> > TDJ::getTableau()
{
    return plateau;
}

void TDJ::init()
{
    std::vector<std::vector<int> > tab;
    tab.resize(8,std::vector<int>(1 << 8, 0));
    tab[3][3]=1;
    tab[3][4]=2;
    tab[4][3]=2;
    tab[4][4]=1;
    tourJoueur= 2;
    setTableau(tab);
    curseur.setx(2);
    curseur.sety(2);
}

int TDJ::getMode()
{
    return Mode;
}

int TDJ::getIA()
{
    return JoueurIA;
}

void TDJ::setMod(int _Mode)
{
    Mode=_Mode;
}

void TDJ::setIA(int _IA)
{
    JoueurIA=_IA;
}

void TDJ::charger()
{
    std::ifstream fichier("sauvegarde.txt", std::ios::in);
    int a;
    if(fichier)
    {
        fichier>>a;
        setJoueur(a);
        fichier>>a;
        setIA(a);
        fichier>>a;
        setMod(a);
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                fichier>>plateau[i][j];
            }
        }
        fichier.close();
    }
    jeu();
}

void TDJ::enregister()
{
    std::ofstream fichier("sauvegarde.txt", std::ios::out | std::ios::trunc);
    if(fichier)
    {
        fichier<<getTourJoueur()<<" "<<getIA()<<" "<<getMode()<<std::endl;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                fichier<<plateau[i][j]<<" ";
            }
            fichier<<std::endl;
        }
        fichier.close();
    }
}

void TDJ::affichageHumain(Console* pConsole,std::list<std::pair<std::pair<int,int>,int> > listCoup)
{
    pConsole->gotoLigCol(0,0);
    std::list<std::pair<std::pair<int,int>,int> >::iterator it;
    std::cout<<"joueur : "<<tourJoueur<<std::endl;
    _afficherTableau(pConsole,0,3);
    TDJ a;
    a.init();
    //a.setJoueur(getTourJoueur());
    a.setTableau(plateau);
    for(it=listCoup.begin(); it!=listCoup.end(); it++)
    {
        if(curseur.getx()==it->first.first&&curseur.gety()==it->first.second)
        {
            a.getSetPlateaux(1,curseur.getx(),curseur.gety());
            a._afficherTableau(pConsole,45,3);
        }
    }

}

std::pair<int,int> TDJ::nbPion()
{
    int blanc=0;
    int noire=0;
    for(int i=0; i<8; i++)
    {
        for(int j=0; j<8; j++)
        {
            if(plateau[i][j]==1)
                blanc++;
            if(plateau[i][j]==2)
                noire++;
        }
    }
    std::pair<int,int> a;
    a.first=blanc;
    a.second=noire;
    return a;
}

int TDJ::_pTableau(int Mode,int x,int y,int dx,int dy)
{
    int poid=0;
    int i=1;
    int exploration=0;

    while(exploration==0)
    {
        if((x+(i*dx))<8 && (x+(i*dx))>-1 && (y+(i*dy))<8 && (y+(i*dy))>-1)
        {
            if(plateau[x+(i*dx)][y+(i*dy)]==0)
            {
                exploration=-1;
            }
            else if(plateau[x+(i*dx)][y+(i*dy)]==getTourJoueur())
            {
                if(i==1)
                {
                    exploration=-1;
                }
                else
                {
                    exploration=1;
                }
            }
            if(plateau[x+(i*dx)][y+(i*dy)]==0)
            {
                exploration=-1;
            }
            else if(plateau[x+(i*dx)][y+(i*dy)]!=getTourJoueur() && plateau[x+(i*dx)][y+(i*dy)]!=0)
            {
                i++;
            }
        }
        else
        {
            exploration=-1;
        }
    }
    while(exploration==1 && i>0)
    {
        if(Mode)
        {
            plateau[x+(i*dx)][y+(i*dy)]=getTourJoueur();
            plateau[x][y]=getTourJoueur();
        }
        if(plateau[x+(i*dx)][y+(i*dy)]!=getTourJoueur())
        {
            poid++;
        }
        i--;
    }
    return poid;
}

int TDJ::getSetPlateaux(int Mode,int x,int y)
{
    int poid=0;
    for(int i=-1; i<2; i++)
    {
        for(int j=-1; j<2; j++)
        {
            //if(i!=0&&j!=0)
            {
                poid=poid+_pTableau(Mode,x,y,i,j);
            }
        }
    }
    /*if(poid)
    {
        std::cout<<poid<<std::endl;
    }*/
    return poid;
}

std::list<std::pair<std::pair<int,int>,int> > TDJ::getListCoupPossible()
{

    std::pair<int,int> pTest;
    std::pair<std::pair<int,int>,int> p1Teste;
    int poid=0;
    std::list<std::pair<std::pair<int,int>,int> > ListCoup;
    for(int i=0; i<8; i++)
    {
        for(int j=0; j<8; j++)
        {
            if(plateau[i][j]==0)
            {
                //std::cout<<i<<":"<<j;
                pTest.first=i;
                pTest.second=j;
                if((poid=getSetPlateaux(0,i,j)))
                {
                    //std::cout<<";"<<std::endl;
                    p1Teste.first=pTest;
                    p1Teste.second=poid;
                    ListCoup.push_back(p1Teste);
                }
                else  //std::cout<<" "<<std::endl;
                {
                }
            }

        }
    }
    return ListCoup;
}

void TDJ::_affichageVictoire()
{
    system("cls");
    std::pair<int,int> compte=nbPion();
    char a='0';
    while(a=='0')
    {
    std::cout<<"Il y'a "<<compte.first<<" pions blancs."<<std::endl;
    std::cout<<"Il y'a "<<compte.second<<" pions noires."<<std::endl;
    if(compte.first>compte.second)
    {
        std::cout<<"Le joueur blanc � gagner!!!!";
    }else{
        std::cout<<"Le joueur noire � gagner!!!!";
    }
    std::cin>>a;
    }
}

void TDJ::jeu()
{
    int finDeJeu=0;
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int antiBug=0;
    bool optionArbre=FALSE;
    while(!finDeJeu)
    {
        if(getTourJoueur()==getIA())
        {
            finDeJeu=tourDeJeuIA(pConsole,optionArbre);
        }
        else
        {
            finDeJeu=tourDeJeuHumain(pConsole,&optionArbre);
        }
        if(finDeJeu==2)
        {
            if(antiBug==1)
            {
                finDeJeu=2;
            }
            else
            {
                antiBug=1;
                finDeJeu=0;
            }
        }
        else
        {
            antiBug=0;
        }
    }
    if(finDeJeu==2)
    {
        _affichageVictoire();
    }
}

int TDJ::tourDeJeuHumain(Console* pConsole,bool* optionArbre)
{
    int EC=0;//entr�e clavier
    int finDeTour=1;
    std::list<std::pair<std::pair<int,int>,int> > ListCoup=getListCoupPossible();
    std::list<std::pair<std::pair<int,int>,int> >::iterator it;
    if(ListCoup.empty())
    {
        finDeTour=2;
        swapTourJoueur();
    }
    while(finDeTour==1)
    {
        if(pConsole->isKeyboardPressed())
        {
            pConsole->deleteInstance();
            EC=pConsole->getInputKey();
            curseur.deplacementCurseur(EC);
            if(EC==13)
            {
                for(it=ListCoup.begin(); it!=ListCoup.end(); ++it)
                {
                    if(it->first.first==curseur.getx()&& it->first.second==curseur.gety()&& it->second>0)
                    {
                        getSetPlateaux(1,curseur.getx(),curseur.gety());
                        finDeTour=0;
                        swapTourJoueur();
                    }
                }
            }
            if(EC==0x4F || EC==0x6F)
            {
                enregister();
            }
            if(EC==0x49 || EC==0x69)
            {
                *optionArbre=(*optionArbre)?FALSE:TRUE;
            }
        }
        if(EC==0x50||EC==0x70)
        {
            finDeTour=-1;
        }
        affichageHumain(pConsole,ListCoup);
    }
    return finDeTour;
}

int TDJ::tourDeJeuIA(Console* pConsole,bool optionArbre)///0 random 2 Minmax
{
    MinMax a;
    a.SetOptionArbre(optionArbre);
    int retour=0;
    std::remove("arbre.txt");
    if(getMode()==0)
    {
        std::pair<int,int> coup;
        coup=a.aleatoire(plateau,getTourJoueur(),pConsole);
        if(coup.second==-1)
        {
            retour=2;
        }
        else
        {
            getSetPlateaux(1,coup.first,coup.second);
        }
    }
    if(getMode()==1)
    {
        std::pair<std::pair<int,int>,int> coup;
        coup=a.minMax(plateau,getTourJoueur(),5);
        if(coup.second==-1)
        {
            retour=2;
        }
        else
        {
            getSetPlateaux(1,coup.first.first,coup.first.second);
        }
    }
    if(getMode()==2)
    {
        std::pair<std::pair<int,int>,int> coup;
        coup=a.meilleurCoup(plateau,getTourJoueur(),3);
        if(coup.second==-1)
        {
            retour=2;
        }
        else
        {
            getSetPlateaux(1,coup.first.first,coup.first.second);
        }
    }
    if(getMode()==3)
    {
        std::pair<std::pair<int,int>,std::pair<int,int> > coup;
        coup=a.alphaBeta(plateau,getTourJoueur(),5,-100000,100000);
        if(coup.second.first==-1)
        {
            retour=2;
        }
        else
        {
            getSetPlateaux(1,coup.first.first,coup.first.second);
        }
    }
    swapTourJoueur();
    return retour;
}

void TDJ::_afficherTableau(Console* pConsole,int n,int m)
{

    for(int i=0;i<17;i++)
    {
        for(int j=0;j<25;j++)
        {
            pConsole->gotoLigCol(m+i,n+j);
            if((j%3)==0&&i==0)  //first line borders
            {
                if(j==0)        //first colon
                {
                    std::cout<<(char)0xC9;
                }
                else if(j==24)  //last colon
                {
                    std::cout<<(char)0xBB;
                }
                else            //colons without the first and last one
                {
                    std::cout<<(char)0xCB;
                }
            }
            else if((j%3)==0&&(i%2)==0&&i!=0&&i!=16) //lines and colons borders, without the first and last one
            {
                if(j==0)       //first colon
                {
                    std::cout<<(char)0xCC;
                }
                else if(j==24) //last colon
                {
                    std::cout<<(char)0xB9;
                }
                else           //colons without the first and last one
                {
                    std::cout<<(char)0xCE;
                }
            }
            else if((j%3)==0&&i==16)     //last line borders
            {
                if(j==0)       //first colon
                {
                    std::cout<<(char)0xC8;
                }
                else if(j==24) //last colon
                {
                    std::cout<<(char)0xBC;
                }
                else           //colons without the first and last one
                {
                    std::cout<<(char)0xCA;
                }
            }
            else if((j%3)!=0&&(i%2)==0) //horizontals borders
            {
                std::cout<<(char)0xCD;
            }
            else if((j%3)==0&&(i%2)!=0) //verticals borders
            {
                std::cout<<(char)0xBA;
            }

            else       //cases
            {

                if(curseur.getx()==((i-1)/2)&&curseur.gety()==((j-1)/3))  //CURSEUR
                {
                    //std::cout<<"";
                    if(plateau[(i-1)/2][(j-1)/3]==1) //whites
                    {
                        pConsole->setColor(12,2);
                        std::cout << "[]" << std::endl;
                        j++;
                        pConsole->setColor(0,15);
                    }
                    else if(plateau[(i-1)/2][(j-1)/3]==2) //blacks
                    {
                        pConsole->setColor(12,14);
                        std::cout << "<>" << std::endl;
                        j++;
                        pConsole->setColor(0,15);
                    }
                    else if(plateau[(i-1)/2][(j-1)/3]==0) //empty cases
                    {
                        pConsole->setColor(12,12);
                        std::cout << "  " << std::endl;
                        j++;
                        pConsole->setColor(0,15);
                    }
                }else{
                if(plateau[(i-1)/2][(j-1)/3]==1) //whites
                {
                    pConsole->setColor(0,2);
                    std::cout << "[]" << std::endl;
                    j++;
                    pConsole->setColor(0,15);
                }
                else if(plateau[(i-1)/2][(j-1)/3]==2) //blacks
                {
                    pConsole->setColor(0,14);
                    std::cout << "<>" << std::endl;
                    j++;
                    pConsole->setColor(0,15);
                }
                else if(plateau[(i-1)/2][(j-1)/3]==0) //empty cases
                {
                    pConsole->setColor(0,0);
                    std::cout << "  " << std::endl;
                    j++;
                    pConsole->setColor(0,15);
                }
                }
            }
        }
    }
}

