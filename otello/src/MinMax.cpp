#include "MinMax.h"

MinMax::MinMax()
{
    //ctor
}

int MinMax::Heuristique(int x,int y,int nbpion)
{
    int a=0;
    if((x==0 && y==0)||(x==0 && y==7)||(x==7 && y==7)||(x==7 && y==0))
    {
        a=nbpion+20;
    }
    else if((x==1 && y==1)||(x==0 && y==1)||(x==1 && y==0)||
            (x==1 && y==6)||(x==0 && y==6)||(x==1 && y==7)||
            (x==6 && y==6)||(x==7 && y==6)||(x==6 && y==7)||
            (x==6 && y==1)||(x==6 && y==0)||(x==7 && y==1))
    {
        a=nbpion-15;
    }
    else
    {
        a=nbpion;
    }
    a=(a<0)?0:a;
    return a;
}

void MinMax::Arbre(int joueur,int x, int y,int profondeur)
{
    std::ofstream fichier("arbre.txt", std::ios::app);
    if(fichier)
    {
        for(int i=1; i<profondeur; i++)
        {
            fichier<<"     ";
        }
        fichier<<joueur<<"-"<<" x:"<<x<<" y:"<<y<<std::endl;
        fichier.close();
    }
}

std::pair<std::pair<int,int>,int> MinMax::calculPoid(std::vector<std::vector <int> >tab,int Joueur,int x,int y,int poid,std::pair<std::pair<int,int>,int> retour)
{
    TDJ tableau;
    tableau.init();
    tableau.setJoueur(Joueur);
    tableau.setTableau(tab);
    std::pair<int,int> nbPion;
    nbPion.first=0;
    nbPion.second=0;
    int entier;

    nbPion=tableau.nbPion();
    if(Joueur==1)
    {
        entier=Heuristique(x,y,nbPion.first);
        if(poid<entier)
        {
            retour.second=entier;
            retour.first.first=x;
            retour.first.second=y;
        }
    }
    else if(Joueur==2)
    {
        entier=Heuristique(x,y,nbPion.second);
        if(poid<entier)
        {
            retour.second=entier;
            retour.first.first=x;
            retour.first.second=y;
        }
    }
    return retour;
}

void MinMax::SetOptionArbre(bool a)
{
    optionArbre=a;
}

MinMax::~MinMax()
{
    //dtor
}

std::pair<int,int> MinMax::aleatoire(std::vector<std::vector <int> >tab,int joueur,Console* pConsole)
{
    TDJ tableau;
    srand (time(NULL));
    tableau.init();
    tableau.setJoueur(joueur);
    tableau.setTableau(tab);
    int choix=0;
    int i=0;
    std::pair<int,int> retour;
    std::list<std::pair<std::pair<int,int>,int> > ListCoup=tableau.getListCoupPossible();
    std::list<std::pair<std::pair<int,int>,int> >::iterator it;
    if(ListCoup.empty())
    {
        retour.second=-1;
        return retour;
    }
    int taille=ListCoup.size();
    choix= rand()%taille;
    for(it=ListCoup.begin(); it!=ListCoup.end(); it++)
    {
        pConsole->gotoLigCol(i,80);
        pConsole->setColor(0,2);
        std::cout<<it->first.first<<":"<<it->first.second;
        pConsole->setColor(0,15);
        if(choix==i)
        {
            retour.first=it->first.first;
            retour.second=it->first.second;
        }
        i++;
    }
    return retour;
}

std::pair<std::pair<int,int>,int> MinMax::minMax(std::vector<std::vector <int> >tab,int joueur,int profondeur)
{
    TDJ tableau;
    tableau.init();
    tableau.setJoueur(joueur);
    tableau.setTableau(tab);
    int meilleurPoid;
    std::list<std::pair<std::pair<int,int>,int> > ListCoup=tableau.getListCoupPossible();
    std::list<std::pair<std::pair<int,int>,int> >::iterator it;
    std::pair<std::pair<int,int>,int> retour;
    retour.first.first=0;
    retour.first.second=0;
    retour.second=0;
    if(ListCoup.empty())
    {
        retour.second=-1;
        return retour;
    }
    for(it=ListCoup.begin(); it!=ListCoup.end(); it++)
    {
        if(optionArbre)
        {
            Arbre(joueur,it->first.first,it->first.second,profondeur);
        }
        tableau.setTableau(tab);
        tableau.getSetPlateaux(1,it->first.first,it->first.second);
        if((profondeur%2)==1)//max
        {
            meilleurPoid=0;
            if(profondeur==1)
            {
                retour=calculPoid(tableau.getTableau(),tableau.getTourJoueur(),it->first.first,it->first.second,meilleurPoid,retour);
            }
            else
            {
                tableau.swapTourJoueur();
                retour=minMax(tableau.getTableau(),tableau.getTourJoueur(),profondeur-1);
                tableau.swapTourJoueur();
            }
            if(retour.second>meilleurPoid)
            {
                meilleurPoid=retour.second;
                retour.first.first=it->first.first;
                retour.first.second=it->first.second;
            }
        }
        if((profondeur%2)==0)//min
        {
            meilleurPoid=100000;
            tableau.swapTourJoueur();
            retour=minMax(tableau.getTableau(),tableau.getTourJoueur(),profondeur-1);
            tableau.swapTourJoueur();
            if(retour.second<meilleurPoid)
            {
                meilleurPoid=retour.second;
                retour.first.first=it->first.first;
                retour.first.second=it->first.second;
            }
        }
    }
    return retour;
}

std::pair<std::pair<int,int>,int> MinMax::meilleurCoup(std::vector<std::vector <int> >tab,int joueur,int profondeur)
{
    TDJ tableau;
    tableau.init();
    tableau.setJoueur(joueur);
    tableau.setTableau(tab);
    int a;
    int poid=-1;
    std::pair<std::pair<int,int>,int> retour;
    std::pair<std::pair<int,int>,int> intermediaire;
    intermediaire.first.first=0;
    intermediaire.first.second=0;
    intermediaire.second=0;
    retour.first.first=0;
    retour.first.second=0;
    retour.second=0;
    std::list<std::pair<std::pair<int,int>,int> > ListCoup=tableau.getListCoupPossible();
    std::list<std::pair<std::pair<int,int>,int> >::iterator it;
    //std::cout<<"                                                 "<<joueur<<":"<<profondeur<<std::endl;
    if(ListCoup.empty())
    {
        retour.second=-1;
        return retour;
    }
    for(it=ListCoup.begin(); it!=ListCoup.end(); it++)
    {
        if(optionArbre)
        {
            Arbre(joueur,it->first.first,it->first.second,profondeur);
        }
        tableau.setTableau(tab);
        tableau.getSetPlateaux(1,it->first.first,it->first.second);
        if(profondeur==1)
        {
            retour=calculPoid(tableau.getTableau(),tableau.getTourJoueur(),it->first.first,it->first.second,poid,retour);
            poid=(retour.second>poid)?retour.second:poid;
        }
        else if(profondeur>1)
        {
            //fais jouer l'adverssaire a son max donc notre min
            tableau.swapTourJoueur();
            intermediaire=meilleurCoup(tableau.getTableau(),tableau.getTourJoueur(),1);
            if(intermediaire.second!=-1)
            {
                tableau.getSetPlateaux(1,intermediaire.first.first,intermediaire.first.second);
            }
            tableau.swapTourJoueur();
            a=intermediaire.second;
            //recurence
            intermediaire=meilleurCoup(tableau.getTableau(),tableau.getTourJoueur(),profondeur-1);
            if(a==-1 && intermediaire.second==-1)
            {
                retour=calculPoid(tableau.getTableau(),tableau.getTourJoueur(),it->first.first,it->first.second,poid,retour);
                poid=(retour.second>poid)?retour.second:poid;
            }
            else
            {
                it->second=intermediaire.second;
                if(it->second>retour.second)
                {
                    retour.first.first=it->first.first;
                    retour.first.second=it->first.second;
                    retour.second=it->second;
                }
            }
        }
    }
    return retour;
}

std::pair<std::pair<int,int>,std::pair<int,int> > MinMax::alphaBeta(std::vector<std::vector <int> >tab,int joueur,int profondeur,int alpha,int beta)
{
    TDJ tableau;
    tableau.init();
    tableau.setJoueur(joueur);
    tableau.setTableau(tab);
    std::pair<std::pair<int,int>,std::pair<int,int> > retour;
    std::pair<std::pair<int,int>,int> intermediaire;
    intermediaire.first.first=0;
    intermediaire.first.second=0;
    intermediaire.second=0;
    std::list<std::pair<std::pair<int,int>,int> > ListCoup=tableau.getListCoupPossible();
    std::list<std::pair<std::pair<int,int>,int> >::iterator it;
    if(!(alpha>=beta))
    {
        if(ListCoup.empty())
        {
            retour.second.first=-1;
            return retour;
        }
        for(it=ListCoup.begin(); it!=ListCoup.end(); it++)
        {
            if(optionArbre)
            {
                Arbre(joueur,it->first.first,it->first.second,profondeur);
            }
            tableau.setTableau(tab);
            tableau.getSetPlateaux(1,it->first.first,it->first.second);
            if(profondeur==1)
            {
                intermediaire=calculPoid(tableau.getTableau(),tableau.getTourJoueur(),it->first.first,it->first.second,intermediaire.second,intermediaire);
                retour.second.first=intermediaire.second;
            }
            if((profondeur%2)==1&&profondeur!=1)//max
            {
                tableau.swapTourJoueur();
                retour=alphaBeta(tableau.getTableau(),tableau.getTourJoueur(),profondeur-1,alpha,beta);
                tableau.swapTourJoueur();

                if(retour.second.first>alpha)
                {
                    alpha=retour.second.first;
                    retour.first.first=it->first.first;
                    retour.first.second=it->first.second;
                }
                retour.second.first=alpha;
                retour.second.second=beta;
            }
            if((profondeur%2)==0)//min
            {
                tableau.swapTourJoueur();
                retour=alphaBeta(tableau.getTableau(),tableau.getTourJoueur(),profondeur-1,alpha,beta);
                tableau.swapTourJoueur();

                if(retour.second.first<beta)
                {
                    beta=retour.second.first;
                    retour.first.first=it->first.first;
                    retour.first.second=it->first.second;
                }
                retour.second.first=alpha;
                retour.second.second=beta;
            }
        }
    }
    if(alpha>=beta)
    {
        retour.second.first=-1;
    }
    return retour;
}
