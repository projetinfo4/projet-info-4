#include "Curseur.h"

Curseur::Curseur()
{
    //ctor
}

Curseur::~Curseur()
{
    //dtor
}

void Curseur::setx(int nx)
{
    x=nx;
    if(x<0)x=0;
    if(x>7)x=7;
}

void Curseur::sety(int ny)
{
    y=ny;
    if(y<0)y=0;
    if(y>7)y=7;
}

int Curseur::getx()
{
    return x;
}

int Curseur::gety()
{
    return y;
}

void Curseur::deplacementCurseur(int EC)//vient de mon projet du premier semestre
{
    if(EC==0x5A||EC==0x7A)
    {
        setx(getx()-1);
    }
    if(EC==0x51||EC==0x71)
    {
        sety(gety()-1);
    }
    if(EC==0x53||EC==0x73)
    {
        setx(getx()+1);
    }
    if(EC==0x44||EC==0x64)
    {
        sety(gety()+1);
    }
}
