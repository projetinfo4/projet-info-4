#include "Menu.h"
#include <iostream>
#include "TDJ.h"
#include <stdlib.h>

Menu::Menu()
{
    //ctor
}

Menu::~Menu()
{
    //dtor
}

void Menu::affMenu()
{
    int choice;
    TDJ dio;
    bool quit = false;
    while( !quit )
    {
        system("cls");
        std::cout<<"________   __  .__           .__  .__             ________                       "<<std::endl;
        std::cout<<"\\_____  \\_/  |_|  |__   ____ |  | |  |   ____    /  _____/_____    _____   ____  "<<std::endl;
        std::cout<<" /   |   \\   __\\  |  \\_/ __ \\|  | |  |  /  _ \\  /   \\  ___\\__  \\  /     \\_/ __ \\ "<<std::endl;
        std::cout<<"/    |    \\  | |   Y  \\  ___/|  |_|  |_(  <_> ) \\    \\_\\  \\/ __ \\|  Y Y  \\  ___/ "<<std::endl;
        std::cout<<"\\_______  /__| |___|  /\\___  >____/____/\\____/   \\______  (____  /__|_|  /\\___  >"<<std::endl;
        std::cout<<"        \\/          \\/     \\/                           \\/     \\/      \\/     \\/ "<<std::endl;
        std::cout<<" "<<std::endl;
        std::cout<<"  1/ J1 VS J2"<<std::endl<<std::endl;
        std::cout<<"  2/ J1 VS IA lvl1"<<std::endl<<std::endl;
        std::cout<<"  3/ J1 VS IA lvl2"<<std::endl<<std::endl;
        std::cout<<"  4/ J1 VS IA lvl3"<<std::endl<<std::endl;
        std::cout<<"  5/ J1 VS IA lvl4"<<std::endl<<std::endl;
        std::cout<<"  6/ Reprendre une partie"<<std::endl<<std::endl;
        std::cout<<"  7/ Quitter"<<std::endl;

        std::cin>>choice;
        if(choice==1||choice==2||choice==3||choice==4||choice==5||choice==6||choice==7)
        {
            switch(choice)
            {
            case 1:
                system("cls");
                //affichage console
                //lancement partie Joueur contre Joueur
                dio.init();
                dio.setMod(0);
                dio.setIA(3);
                dio.jeu();
                break;

            case 2:
                system("cls");
                //affichage console
                //lancement partie Joueur contre IA de niveau 1

                dio.init();
                dio.setMod(0);
                dio.setIA(1);
                dio.jeu();
                break;

            case 3:
                system("cls");
                //affichage console
                //lancement partie Joueur contre IA de niveau 2
                dio.init();
                dio.setMod(1);
                dio.setIA(1);
                dio.jeu();
                break;

            case 4:
                system("cls");
                //affichage console
                //lancement partie Joueur contre IA de niveau 3
                dio.init();
                dio.setMod(2);
                dio.setIA(1);
                dio.jeu();
                break;
            case 5:
                system("cls");
                //affichage console
                //lancement partie Joueur contre IA de niveau 3
                dio.init();
                dio.setMod(3);
                dio.setIA(1);
                dio.jeu();
                break;
            case 6:
                system("cls");
                dio.init();
                dio.charger();
                break;
            case 7:
                std::cout<<"          ___.   .__               __          __   "<<std::endl;
                std::cout<<" _____    \\_ |__ |__| ____   _____/  |_  _____/  |_ "<<std::endl;
                std::cout<<" \\__  \\    | __ \\|  |/ __ \\ /    \\   __\\/  _ \\   __\\ "<<std::endl;
                std::cout<<"  / __ \\_  | \\_\\ \\  \\  ___/|   |  \\  | (  <_> )  |  "<<std::endl;
                std::cout<<" (____  /  |___  /__|\\___  >___|  /__|  \\____/|__|  "<<std::endl;
                std::cout<<"      \\/       \\/        \\/     \\/                  "<<std::endl;

                quit = true;
                break;
            }
        }
    };


}
