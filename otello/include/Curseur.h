#ifndef CURSEUR_H
#define CURSEUR_H


class Curseur
{
    public:
        Curseur();
        virtual ~Curseur();
        ///permet le d�placement du curseur
        void deplacementCurseur(int EC);
        void setx(int nx);
        void sety(int ny);
        int getx();
        int gety();
    protected:
    private:
        ///coordon�e x du curseur
        int x;
        ///coordon�e y du curseur
        int y;
};

#endif // CURSEUR_H
