#ifndef MINMAX_H
#define MINMAX_H
#include "TDJ.h"
#include <stdlib.h>
#include <time.h>
#include <console.h>

class MinMax
{
    public:
        MinMax();
        virtual ~MinMax();
        void SetOptionArbre(bool a);
        ///permet la gestion de l'heuristique
        int Heuristique(int x,int y,int nbpion);
        ///affichage de l'arbre dans un fichiuer texte
        void Arbre(int joueur,int x, int y,int profondeur);
        ///calcul de poid
        std::pair<std::pair<int,int>,int> calculPoid(std::vector<std::vector <int> >tab,int Joueur,int x,int y,int poid,std::pair<std::pair<int,int>,int> retour);
        /// IA aleatoire
        std::pair<int,int> aleatoire(std::vector<std::vector <int> >tab,int joueur,Console* pConsole);
        /// IA MinMax
        std::pair<std::pair<int,int>,int> minMax(std::vector<std::vector <int> >tab,int joueur,int profondeur);
        /// IA optimis�e
        std::pair<std::pair<int,int>,int> meilleurCoup(std::vector<std::vector <int> >tab,int joueur,int profondeur);
        /// IA alpha beta
        std::pair<std::pair<int,int>,std::pair<int,int> > alphaBeta(std::vector<std::vector <int> >tab,int joueur,int profondeur,int alpha,int beta);
    protected:
    private:
        ///Autorise ou non la cr�ation d'arbre complexe
        bool optionArbre;
};

#endif // MINMAX_H
