#ifndef TDJ_H
#define TDJ_H
#include <vector>
#include <string>
#include <windows.h>
#include <mmsystem.h>
#include <list>
#include <fstream>
#include <utility>
#include "MinMax.h"
#include "Curseur.h"
#include "console.h"

class TDJ
{
    public:
        TDJ();
        TDJ(std::vector<std::vector <int> >_plateau,int _tourJoueur);
        virtual ~TDJ();
        ///initialisation du jeu
        void init();
        ///permet d'enregistrer la partie
        void enregister();
        ///permet de charger la partie enregistrer
        void charger();
        ///recup�re le niveau de l'IA
        int getMode();
        ///r�cup�re la couleur de l'IA
        int getIA();
        ///fixe le niveau de l'IA
        void setMod(int _Mode);
        ///fixe la couleur de l'IA
        void setIA(int _IA);
        ///indique qui doit jouer
        int getTourJoueur();
        ///clos le tour d'un joueur et ouvre celui de l'autre
        void swapTourJoueur();
        ///fixe le joueur qui doit
        void setJoueur(int joueur);
        ///change le tableau
        void setTableau(std::vector<std::vector <int> > tableau);
        ///r�cup�re le tableau
        std::vector<std::vector <int> > getTableau();
        ///compte les pions du tableau et renvoie une paire {blancs;noires}
        std::pair<int,int> nbPion();
        ///affiches le jeu � l'�cran
        void affichageHumain(Console* pConsole,std::list<std::pair<std::pair<int,int>,int> > listCoup);
        ///focntion de parcours du tableau utilis� pour retourner les pions et faire la liste de coup(Mode=0:explorer 1:remplacer)
        int getSetPlateaux(int Mode,int x,int y);
        ///tout ce passe dedans
        void jeu();
        ///r�cup�re une liste de coup jouable {{x;y};poid}
        std::list<std::pair<std::pair<int,int>,int> > getListCoupPossible();
        ///Fait jouer un �tre humain
        int tourDeJeuHumain(Console* pConsole,bool* optionArbre);
        ///Fait joueur l'ordinateur
        int tourDeJeuIA(Console* pConsole,bool optionArbre);
    protected:
    private:
        ///tableau d'int tel que 0=vide 1=blanc 2=noire
        int _pTableau(int mode,int x,int y,int dx,int dy);
        ///affiche l'�cran de victoire
        void _affichageVictoire();
        ///affiche le tableau pour l'affichage
        void _afficherTableau(Console* pConsole,int n,int m);

        ///le fameux plateaux de jeu
        std::vector<std::vector <int> >plateau;
        ///0 aleatoire 1 NegaMax
        int Mode;
        ///3humain vs humain ,2 IA noire,1 IA blanc
        int JoueurIA;
        ///1 blanc 2 noire
        int tourJoueur;
        ///un curseur pour joueur en tant qu'humain
        Curseur curseur;
};

//0 : vide
//1 : blanc
//2 : noire

#endif // TDJ_H
